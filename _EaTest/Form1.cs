﻿using System.Windows.Forms;

namespace _EaTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();  
        }

        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Right))
            {
                //Close the form or the app on mouse right click
                this.Close();
            }
            else
            {
                //Just make the form on top of all of the showing in your desktop.
                this.Show();
                this.Activate();
                this.WindowState = FormWindowState.Normal;
            }
        }
    }
}
